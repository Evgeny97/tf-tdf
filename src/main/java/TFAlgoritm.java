import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;

public class TFAlgoritm {
    private ArrayList<Document> documents = new ArrayList<>();
    private AllDocuments allDocuments = new AllDocuments();


    void readAllDocs() {
        File folder = new File("docs");
        File[] files = folder.listFiles();
        if (files != null) {
            for (File file : files) {
                try (InputStream inputStream = new FileInputStream(file)) {
                    Document document = new Document(file.getName());
                    documents.add(document);
                    Scanner scanner = new Scanner(inputStream, StandardCharsets.UTF_8.name());
                    scanner.useDelimiter("[^a-zA-Zа-яА-ЯёЁ\\-\"]+");
                    while (scanner.hasNext()) {
                        String next = scanner.next();
                        document.addWord(next.toLowerCase());
                    }
                    allDocuments.addDocument(document);
                    document.countAllWordsTFs();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            allDocuments.countAllWordsIDFs();
            for (Document document : documents) {
                document.countAllWordsTF_IDFs(allDocuments);
                document.sort();
                document.printWordTF_IDF(5);
            }

        }
    }
}
