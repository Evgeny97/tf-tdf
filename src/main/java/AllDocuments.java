//Created by Evgeny on 16.12.2017.

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class AllDocuments {
    private ArrayList<Document> documentsList;
    private Map<String, Double> wordsIDFs = new HashMap<>();

    public AllDocuments() {
        this.documentsList = new ArrayList<>();
    }

    void countAllWordsIDFs() {
        for (Map.Entry<String, Double> entry : wordsIDFs.entrySet()) {
            entry.setValue(Math.log(documentsList.size() / entry.getValue()));
        }
    }

    public Double getIDF(String word){
        return wordsIDFs.get(word);
    }


    void addDocument(Document document) {
        documentsList.add(document);
        Iterator<Map.Entry<String, Double>> iterator = document.getIterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Double> entry = iterator.next();
            Double numberOfDocuments;
            if ((numberOfDocuments = wordsIDFs.get(entry.getKey())) != null) {
                wordsIDFs.put(entry.getKey(), numberOfDocuments + 1);
            } else {
                wordsIDFs.put(entry.getKey(), 1.0);
            }
        }
    }
}
