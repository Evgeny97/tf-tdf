import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Document {
    String name;
    private Map<String, Double> wordsTFs = new HashMap<>();
    private int wordsNumber = 0;

    public Document(String name) {
        this.name = name;
    }

    void sort() {
        wordsTFs = MapUtil.sortByValue(wordsTFs);
    }

    void printWordTF_IDF() {
        for (Map.Entry<String, Double> entry : wordsTFs.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }
    }

    void countAllWordsTFs() {
        for (Map.Entry<String, Double> entry : wordsTFs.entrySet()) {
            entry.setValue(entry.getValue() / wordsNumber);
        }
    }

    void countAllWordsTF_IDFs(AllDocuments allDocuments) {
        for (Map.Entry<String, Double> entry : wordsTFs.entrySet()) {
            entry.setValue(entry.getValue() * allDocuments.getIDF(entry.getKey()));
        }
    }

    void printWordTF_IDF(int maxWords) {
        System.out.println("In document: " + name);
        for (Map.Entry<String, Double> entry : wordsTFs.entrySet()) {
            if (maxWords == 0) {
                break;
            }
            System.out.println("Word's: " +entry.getKey() + " tf-idf:  " + entry.getValue());
            maxWords--;
        }
    }

    Iterator<Map.Entry<String, Double>> getIterator() {
        return wordsTFs.entrySet().iterator();
    }

    void addWord(String word) {
        Double frequency;
        if ((frequency = wordsTFs.get(word)) != null) {
            wordsTFs.put(word, frequency + 1);
        } else {
            wordsTFs.put(word, 1.0);
        }
        wordsNumber++;
    }
}
